import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'Teomedia';
	isActive = false;
	isOpen = false;

	infomationClick(e) {
		this.isActive = !this.isActive;
	}

	contactClick(e) {
		this.isOpen = !this.isOpen;
	}
}
